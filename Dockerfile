FROM php:7.4-fpm-alpine

ARG VERSION=1.4.0

RUN apk add --no-cache rsync
RUN set -ex; \
    \
    apk add --no-cache --virtual .build-deps \
        freetype-dev \
        libjpeg-turbo-dev \
        libwebp-dev \
    ; \
    \
    docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp; \
    docker-php-ext-install -j $(nproc) \
        gd \
        opcache \
    ; \
    \
    runDeps="$( \
        scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
            | tr ',' '\n' \
            | sort -u \
            | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    )"; \
    apk add --virtual .privatebin-phpext-rundeps $runDeps; \
    apk del .build-deps

# set recommended PHP.ini settings
RUN echo 'memory_limit=128M' > /usr/local/etc/php/conf.d/memory-limit.ini; \
    printf 'upload_max_filesize=10M\npost_max_size=10M\n' > /usr/local/etc/php/conf.d/upload-limit.ini; \
    \
    chown -R www-data:root /var/www; \
    chmod -R g=u /var/www

VOLUME /var/www/html /var/www/html/cfg /var/www/html/data

RUN set -ex; \
    \
    apk add --no-cache --virtual .fetch-deps gnupg; \
    export GNUPGHOME="$(mktemp -d)"; \
    curl -fsSL "https://github.com/PrivateBin/PrivateBin/archive/$VERSION.tar.gz" -o PrivateBin.tar.gz; \
    curl -sSL https://privatebin.info/key/release.asc | gpg2 --import -; \
    curl -sSL "https://github.com/PrivateBin/PrivateBin/releases/download/$VERSION/PrivateBin-$VERSION.tar.gz.asc"|gpg2 --verify - PrivateBin.tar.gz ;\
    mkdir -p /usr/src/PrivateBin; \
    tar -xzf PrivateBin.tar.gz -C /usr/src/PrivateBin --strip 1; \
    rm -f PrivateBin.tar.gz; \
    rm -rf "$GNUPGHOME"; \
    apk del .fetch-deps
   
COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
CMD ["php-fpm"]

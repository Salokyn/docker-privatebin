#!/bin/sh
# Author Nicolas Gif
# Greatly inspired from https://github.com/nextcloud/docker/blob/master/17.0/fpm/entrypoint.sh

# Set password
if [ -w /var/www/html/cfg/conf.php ]; then
  if [ -n "$DB_PASSWORD" ]; then
    sed -i "s/\(^pwd\s*=\s*\).*/\1${DB_PASSWORD}/g" /var/www/html/cfg/conf.php
  elif [ -n "$DB_PASSWORD_FILE" ]; then
    sed -i "s/\(^pwd\s*=\s*\).*/\1$(cat "${DB_PASSWORD_FILE}")/g" /var/www/html/cfg/conf.php
  fi
fi

set -ex

# version_greater A B returns whether A > B
version_greater() {
  [ "$(printf '%s\n' "$@" | sort -t '.' -n -k1,1 -k2,2 -k3,3 | head -n 1)" != "$1" ]
}

installed_version="0.0.0"
if [ -f /var/www/html/lib/Controller.php ]; then
  installed_version="$(php -r 'require("/var/www/html/lib/Controller.php"); echo PrivateBin\Controller::VERSION;')"
fi

image_version="$(php -r 'require("/usr/src/PrivateBin/lib/Controller.php"); echo PrivateBin\Controller::VERSION;')"

if version_greater "$installed_version" "$image_version"; then
  echo "Installed version ($installed_version) is greater than image version ($image_version)" >&2
  exit 1
fi

if version_greater "$image_version" "$installed_version"; then

  if [ "$(id -u)" = 0 ]; then
    rsync_options="-rlDog --chown www-data:root"
  else
    rsync_options="-rlD"
  fi

  # shellcheck disable=SC2086
  rsync $rsync_options --delete --exclude "/data/" --exclude "/cfg/" /usr/src/PrivateBin/ /var/www/html/

  if [ ! -d "/var/www/html/tpl" ] || [ -z "$(ls -A "/var/www/html/tpl")" ]; then
    # shellcheck disable=SC2086
    rsync $rsync_options --include "/tpl/" --exclude '/*' /usr/src/PrivateBin/ /var/www/html/
  fi
fi
chown -R www-data:root /var/www/html/data

exec "$@"
